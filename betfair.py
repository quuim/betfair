#!/usr/bin/env python2
# -*- coding:utf-8 -*-

import re, json
import requests

regexMarketsID = '/exchange/plus/\\#/%s/market/([0-9\\.]{0,})'
regexEventIDfromURL = 'id=([0-9]{0,})'
#regexEventIDfromHTML = '/exchange/%s/event?id=([0-9]{0,})'
regexEventIDfromHTML = 'data-eventid=\"([0-9]{0,})\"'


apiBaseURL = 'https://uk-api.betfair.es/www/sports/exchange/readonly/v1.0/bymarket?currencyCode=EUR&alt=json&locale=es&types=MARKET_STATE,MARKET_RATES,MARKET_DESCRIPTION,EVENT,RUNNER_DESCRIPTION,RUNNER_STATE,RUNNER_EXCHANGE_PRICES_BEST,RUNNER_METADATA,RUNNER_SP&marketIds='
eventBaseURL = 'https://www.betfair.es/exchange/%s/event?id=%s'
mainURL = "https://www.betfair.es/exchange/%s/"

"""
Tipos de mercados:
CORRECT_SCORE
MATCH_ODDS
OVER_UNDER_15
OVER_UNDER_25
OVER_UNDER_35
"""

class Manager:
	def __init__(self, categ):
		"""Constructor.

		Args:
			categ: Categoría de la cual buscar eventos (football, tennis...)

		Raises:
			TimeoutError: Si no hay red
		"""
		self.categ = categ
		self.__url = mainURL % self.categ
		self.__existingEvents = []
		self.__events = []

		if not self.refresh():
			raise Exception("Timeout")

	def __getEvents(self):
		p = re.compile(regexEventIDfromHTML) 
		eventsIDList = p.findall(self.__data)

		events = []
		#TODO: Crear los objetos es algo lento, depende el numero de eventos que haya, mirar de optimizarlo (si se puede)
		#TODO: limpiar self.__existingEvents de los eventos que ya no existen
		for i in range(len(eventsIDList)):
			ID = eventsIDList[i]
			if ID not in self.__existingEvents:
				events.append(
					Event(categ=self.categ, ID=ID)
				)
				self.__existingEvents.append(ID)
			else:
				#Evitamos poner dos veces un mismo evento, que a veces salen duplicados no se muy bien porque
				if not ID in eventsIDList[:i]:
					events.append(self.__events[i])
				else:
					#ID duplicada
					pass

		self.__events = events
	
	def refresh(self, recursive=False):
		"""Recarga los datos, y vuelve a cargar los eventos disponibles

		Args:
			recursive: Si es true hará un refresh de todos los eventos, si está en false no

		Returns:
			True si no ha errores, false si los hay
		"""
		headers = {
			#Parece que no comprueban nada, pero por si acaso
			'Content-Type':      'application/json',
			'User-Agent':        'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
			'Referer':           'https://uk-api.betfair.es/data/DataFrame.html?dom=betfair.es',
			'RX-Requested-With': 'XMLHttpRequest'
		}
		try:
			self.__data = requests.get(self.__url, headers=headers).text
			self.__getEvents()
		except Exception as e:
			print "[FATAL] Timeout Error"
			return False
		#TODO: Ralmente necesario? ya se hace un refresh al contructor
		if recursive:
			for event in self.__events:
				event.refresh()

		return True

	def refreshEvents(self):
		"""Recarga los datos de todos los eventos
		"""
		for event in self.__events:
			event.refresh()


	def events(self):
		return self.__events


class Event:
	def __init__(self, eventUrl = None, categ = None, ID = None):
		"""Constructor.
	
		Args:
			eventUrl: URL completa del evento, incluido el http(s)://
			categ: Categoría del evento cuando no se pasa una url
			ID: id del evento cuando no se pasa una url

		Raises:
			Exception: Si no se pasa ningún parámetro al contructor
			TimeoutError: Si no se pueden cargar los datos devido a que no hay red
		"""
		if categ is None or ID is None:
			if not eventUrl is None:
				p = re.compile(regexEventIDfromURL)
				self.__eventID = p.findall(eventUrl)[0]
				self.url = self.__createApiURL(eventUrl)
			else:
				raise Exception('Tienes que pasar una url o la categoría+ID')
		else:
			self.__eventID = ID
			self.url = self.__createApiURL(eventBaseURL % (categ, ID))
		
		if not self.refresh():
			raise Exception("Timeout")

	def __createApiURL(self, eventUrl):
		"""Crea la URL de la API, donde está la información en formato JSON,
		a partir de la URL del evento.
		
		Args:
			eventUrl: URL completa del evento, incluido el http(s)://
						
		Returns:
			(string) URL de la API 
		"""
		eventHTML = requests.get(eventUrl).text
		categ = eventUrl.split('/')[4]
		p = re.compile(regexMarketsID % categ)
		marketsList = p.findall(eventHTML)
		return apiBaseURL + ','.join((marketsList[i] for i in range(len(marketsList) / 2)))

	def refresh(self):
		"""Descarga los datos que contiene la URL de la API, y los carga en formato JSON
		en self.__data
		"""
		headers = {
			#Parece que no comprueban nada, pero por si acaso
			'Content-Type':      'application/json',
			'User-Agent':        'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
			'Referer':           'https://uk-api.betfair.es/data/DataFrame.html?dom=betfair.es',
			'RX-Requested-With': 'XMLHttpRequest'
		}
		try:
			betData = requests.get(self.url, headers=headers).text
			self.__data = json.loads(betData)
			return True
		except:
			print "[FATAL] Timeout Error"
			return False

	def markets(self):
		"""Devuelve una lista con los mercados que forman este evento
		
		Returns:
			(list[Market]) Lista de mercados (clase Market) que componen este evento
		"""
		markets = []
		for i in range(len(self.__data['eventTypes'][0]['eventNodes'][0]['marketNodes'])):
			markets.append(Market(i, self))

		return markets

	def id(self):
		"""Devuelve el ID del evento
		"""
		return self.__eventID

	def name(self):
		"""Da el nombre del evento
		
		Returns:
			Nombre del evento (string)
		"""
		return self.__data['eventTypes'][0]['eventNodes'][0]['event']['eventName']

	def isInPlay(self):
		"""Dice si el partido está en juego o no
		
		Returns:
			(bool) True si está en juego, False si no lo está
		"""
		if self.__data['eventTypes'][0]['eventNodes'][0]['marketNodes'][0]['state']['inplay'] == 'true':
			return True
		return False

	def getOpenDate(self):
		"""Devuelve el día y hora en que se abre un evento.
		
		Returns:
			(list) Fecha cuando se abre el evento en formato ['2016-06-29', '22:30:00.000']
		"""
		lista = []
		lista.append(self.__data['eventTypes'][0]['eventNodes'][0]['event']['openDate'].split('T')[0])
		lista.append(self.__data['eventTypes'][0]['eventNodes'][0]['event']['openDate'].split('T')[1][:-1])
		return lista

	def country(self):
		"""Devuelve el país del encuentro (por ejemplo ES, FR...)
		
		Returns:
			Código de país del encuentro
		"""
		return self.__data['eventTypes'][0]['eventNodes'][0]['event']['countryCode']

	def getData(self):
		"""Devuelve la cadena JSON con todos los datos
		"""
		return self.__data

class Market:
	def __init__(self, marketIndex, event):
		self.__event = event
		self.__marketIndex = marketIndex
		self.__name = self.__marketName()
		self.__ID = self.__marketID()

	def __marketName(self):
		return self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['description']['marketName']

	def __marketID(self):
		return self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['marketId']

	def name(self):
		"""Devuelve el nombre del mercado
		"""
		return self.__name

	def type(self):
		"""Devuelve el tipo, que es con lo que identifica betfair el tipo de mercado
		
		Returns.
			(string) Identificador del tipo de mercado
		"""
		return self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['description']['marketType']

	def id(self):
		"""Devuelve el ID del mercado
		"""
		return self.__ID

	def totalMatched(self):
		"""Devuelve el nmero de € igualados en este mercado
		
		Returns:
			(float) € igualados en el mercado actual
		"""
		return self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['state']['totalMatched']

	def totalAvailable(self):
		"""Devuelve el número de € disponibles en apuestas en este mercado
		
		Returns:
			(float) € disponibles en el mercado actual
		"""
		return self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['state']['totalAvailable']

	def open(self):
		"""Indica si un mercado está abierto o no
		
		Returns:
			(bool) True si está abierto, False si no lo está
		"""
		if self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['state']['status'] == 'OPEN':
			return True
		return False

	def numOfElements(self):
		"""Devuelve el número de elementos del mercado (p. ej. en Over/Under 1.5 devolvería 2)
		
		Returns:
			(int) Número de elementos en el mercado indicado por el índice.
						"""
		return self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['state']['numberOfRunners']

	def elements(self):
		"""Devuelve una lista con los elementos que forman el mercado actual
		
		Returns:
			(list[Element]) Lista de elementos (clase Element) que forman el mercado actual
		"""
		elementos = []
		for i in range(self.numOfElements()):
			elementos.append(Element(self.__marketIndex, i, self.__event))

		return elementos

class Element:
	def __init__(self, marketIndex, elementIndex, event):
		self.__event = event
		self.__marketIndex = marketIndex
		self.__elementIndex = elementIndex
		self.__name = self.__elementName()

	def __elementName(self):
		"""Devuelve el nombre del elemento, marcado por elementIndex, del mercado marcado por marketIndex.
		Los indices son ints que indican la posición del mercado o elemento, y empiezan desde 0.
		
		Args:
			marketIndex: Índice del mercado en la cadena JSON, empezando por 0
			elementIndex: Índice del elemento del mercad en la cadena JSON, empezando por 0
		
		Returns:
			(string) Nombre el elemento del mercado, por ejemplo 'Más de 1.5 Goles', o 'Empate'.
		"""
		return self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['description']['runnerName']

	def name(self):
		return self.__name

	def backOdds(self):
		"""Devuelve todas las cuotas disponibles para hacer back, en orden
		
		Returns:
			(list[float]) Lista que contiene todas las cuotas del Back ordenada, por ejemplo [1.23, 1.21, 1.19, 1.187]
		"""
		odds = []

		for i in range(len(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToBack'])):
			odds.append(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToBack'][i]['price'])

		return odds



	def layOdds(self):
		"""Devuelve todas las cuotas disponibles para hacer lay, en orden
		
		Returns:
			(list[float]) Lista que contiene todas las cuotas del lay ordenadas, por ejemplo [1.4, 1.42, 1.46, 1.5]
		"""
		odds = []

		for i in range(len(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToLay'])):
			odds.append(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToLay'][i]['price'])

		return odds



	def backSize(self):
		"""Devuelve el volumen disponible para apuestas a favor de un elemento en €
		
		Returns:
			(list[float]) Lista de cantidades disponibles para apostar a favor
		"""
		sizes = []

		for i in range(len(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToBack'])):
			sizes.append(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToBack'][i]['size'])

		return sizes



	def laySize(self):
		"""Devuelve el volumen disponible para apuestas en contra de un elemento en € 
	
		Returns:
			(list[float]) Lista de cantidades disponibles para apostar en contra
		"""
		sizes = []
		for i in range(len(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToLay'])):
			sizes.append(self.__event.getData()['eventTypes'][0]['eventNodes'][0]['marketNodes'][self.__marketIndex]['runners'][self.__elementIndex]['exchange']['availableToLay'][i]['size'])

		return sizes
